package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    Random random = new Random();

    public void run() {
        boolean continuePlaying = true;

        while (continuePlaying) {
            // System.out.println("Continue: " + continuePlaying);
            System.out.println("Let's play round " + roundCounter);

            String humanChoice = userChoice();

            String computerChoice = randomChoice();

            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins!");
                humanScore++;
            } else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins!");
                computerScore++;
            } else {
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continueString = continuePlaying();

            if (continueString.equals("n")) {
                continuePlaying = false;
            }
            roundCounter++;
        }
        System.out.println("Bye bye :)");
    }

    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            } else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }
        }
    }

    public String randomChoice() {
        return rpsChoices.get(random.nextInt(3));
    }

    public boolean validateInput(String choice, List<String> choices) {
        return choices.contains(choice);
    }

    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return (choice2.equals("rock"));
        } else if (choice1.equals("scissors")) {
            return (choice2.equals("paper"));
        } else {
            return (choice2.equals("scissors"));
        }
    }

    public String continuePlaying() {
        List<String> validChoices = Arrays.asList("y", "n");

        while (true) {
            String humanChoice = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();

            if (validateInput(humanChoice, validChoices)) {
                return humanChoice;
            } else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
